﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour{

    [SerializeField]
    private Vector3 axes;     //posibles ejes escalados
    public float scaleUnits;  //velocidad de escalado

    //Update is called once per frame

    void Update()
    {
        //Acotacion de los valores de escalado al valor unitario[-1,1]
        axes = CapsuleMovement.ClampVector3(axes);

        //La escale,alcontrarioque la rotacion y el movimineto,es acumulativa
        //lo que quiere decir que debemos añadir el nuevo valor de escala,al valor anterior.

        transform.localScale += axes * (scaleUnits * Time.deltaTime);



    }


     
}
